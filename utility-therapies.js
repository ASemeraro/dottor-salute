/**
 * A module that wraps up all the methods related to the therapies and their manipolation.
 * @module utility-therapies
 * @author Alessandra Semeraro
 */

/** 
 * Import the Dialogflow module and response creation dependencies 
 * from the Actions on Google client library.
 */ 
const {
    Image,
    BrowseCarousel,
    BrowseCarouselItem,
} = require('actions-on-google');

const ud = require('./utility-date');
const dialogs = require('./dialog-strings');

module.exports = {

    /**
     * Gets all the medications that the user has not taken yet.
     * 
     * @param {Array<Object>} todayT array with today's medications.
     * @param {String} currentDate string with the current date (DD/MM/YYYY).
     * @returns an Array with medications' objects.
     */
    getTodaysNotTakenMeds: function (todayT, currentDate) {
        let arrMedicationsInfo = [];
        for (const t of todayT) {
            for (const o of t.dettaglioAssunzione.orario) {
                if (o.assunto === false) {
                    arrMedicationsInfo.push({
                        assunto : o.assunto,
                        id: t.id,
                        data: currentDate,
                        nome: t.farmaco,
                        ora: o.ora,
                        oraAssunzione: o.ora,
                        dose: t.dettaglioAssunzione.dose,
                        tipoDose: dialogs.getPluralFromWordAndQuantity(t.dettaglioAssunzione.tipoDose, t.dettaglioAssunzione.dose),
                    })
                }
            }
        }
        arrMedicationsInfo.sort(this.sortTherapiesByTime);
        console.log(arrMedicationsInfo);
        return arrMedicationsInfo;
    },

    /**
     * Gets the remainders for the medications that the user has not taken and are late.
     * 
     * @param {Array<Object>} notTakenMedications an array with all the medications the user has not taken yet.
     * @param {String} currentDate a string with the current date (DD/MM/YYYY).
     * @returns an Array with all the medications to be remainded.
     */
    getMedRemindersFromTodayNotTakenMeds: function (notTakenMedications, currentDate) {
        let medicationsIDs = [];
        if (notTakenMedications.length > 0) {
            for (const m of notTakenMedications) {
                if (ud.isAfterHour(currentDate, m.ora)) {
                    // Will keep the order
                    medicationsIDs.push(m);
                }
            }
        }
        return medicationsIDs;
    },

    /**
     * Gets a carousel with the medications passed as a parameter.
     * 
     * @param {Array<Object>} medications array with medication objects, from which the informations for
     * the carousel will be extracted.
     * @returns a BrowseCarousel with the medications' informations.
     */
    getMedsToTakeCarousel: function (medications) {
        for (const m of medications) {
            itemsArr.push(new BrowseCarouselItem({
                title: `${m.dose} ${m.tipoDose} di ${m.nome}`,
                description: `Da prendere alle ${m.ora}`,
                footer: `Scopo: ${m.scopo}`,
                image: new Image({
                    alt: m.nome,
                    url: `https://www.my-personaltrainer.it/thumbs/700x700/https://www.my-personaltrainer.it/images/farmaci/${m.nome}.jpg`
                }),
                url: `https://www.my-personaltrainer.it/Foglietti-illustrativi/${m.nome}.html`,
            }));
        }

        return new BrowseCarousel({
            items: itemsArr,
        });
    },
    

    /**
     * Sets the value of `assunto` to true for those medications in therapies that the user has taken.
     * 
     * @param {Array<Object>} therapies array with today's therapies.
     * @param {Array<Object>} ids array with medication objects.
     * @returns an Array of objects with the updated therapies.
     */
    takeMedicationsById: function (therapies, ids) {
        for (const i of ids) {
            // For each object in ids {id: 'AAA', ora: '9:00'}, find the therapy with said id and change the 'assunto' property
            let index = therapies.findIndex(t => t.id === i.id);
            (therapies[index].dettaglioAssunzione.orario.find(o => o.ora === i.ora)).assunto = true;
        }
        return therapies;
    },

    /**
     * Sets the value of `assunto` to 'non assunto' for those medications in therapies that the user has NOT taken.
     * 
     * @param {Array<Object>} therapies array with today's therapies.
     * @param {Array<Object>} ids array with medication objects.
     * @returns an Array of objects with the updated therapies.
     */
    refuseMedicationsById: function (therapies, ids) {
        for (const i of ids) {
            // For each object in ids {id: 'AAA', ora: '9:00'}, find the therapy with said id and change the 'assunto' property
            let index = therapies.findIndex(t => t.id === i.id);
            (therapies[index].dettaglioAssunzione.orario.find(o => o.ora === i.ora)).assunto = "non assunto";
        }
        return therapies;
    },

    /**
     * Sorting method for an array of medications.
     * 
     * @param {Object} a first object.
     * @param {Object} b second object.
     * @returns a Number - `-1` if `a < b`; `1` if `a > b`; `0` if `a == b`.
     */
    sortTherapiesByTime: function (a, b) {
        if (!ud.isAfterHour(null, a.ora, b.ora))
            return -1;
        if (ud.isAfterHour(null, a.ora, b.ora))
            return 1;
        return 0;
    },

    /**
     * Gets the time associated to the given medications.
     * 
     * @param {Array<Object>} objArr array of medication objects.
     * @returns an Array with the time values extracted from the medication objects.
     */
    getTimeFromMedicationObjects: function (objArr) {
        let timeArr = [];
        for (const t of objArr) {
            timeArr.push(t.ora);
        }
        return timeArr;
    },

    /**
     * Calculates the difference between two given sets of medication objects.
     * BEWARE: It doesn't take the 'oraAssunzione' property into account! That is
     * because what identifies uniquely medication objects is the properties
     * 'id' and 'ora'.
     * @param {Set} a fisrt set of medication objects.
     * @param {Set} b second set of medication objects.
     * @returns an Array with the elements resulting from the substraction of a and b.
     */
    getMedicationsFromFirstSet: function (a, b) {
        let difference = new Set(
            [...a].filter(x => {
                var arrB = [...b];
                var arrFound = arrB.filter(m => (m.id === x.id && m.ora === x.ora));
                // If the object is not in the second array (A - B means every element of A that
                // is not in B)
                if (arrFound.length === 0) {
                    return true;
                } else {
                    return false;
                }
            }));
        return Array.from(difference);
    },

    /**
     * Finds the medication by name and time.
     * 
     * @param {Array<Object>} arrMeds array of medications.
     * @param {String} name name of the medication we want to find in arrMeds.
     * @param {String} time time of the medication we want to find in arrMeds.
     * @returns an Object with the found medication, or `undefined` if no medication was found.
     */
    findMedicationByNameAndTime: function (arrMeds, name, time) {
        let medWithNameAndTimeBefore = arrMeds.filter(m => (m.nome === name && ud.isSameOrBeforeHour(m.ora, time))); //[{object1}]
        let medWithNameAndTimeAfter = arrMeds.filter(m => (m.nome === name && ud.isAfterHour(null, m.ora, time))); //[{object1}]
        // Last element
        if (medWithNameAndTimeBefore.length > 0) {
            let lastIndex = medWithNameAndTimeBefore.length - 1;
            return medWithNameAndTimeBefore[lastIndex];
        } else if (medWithNameAndTimeAfter.length > 0) {
            return medWithNameAndTimeAfter[0];
        } else {
            return undefined;
        }
    },

    /**
     * Finds the medication(s) by name.
     * 
     * @param {Array<Object>} arrMeds array of medications.
     * @param {String} name name of the medication(s) we want to find in arrMeds.
     * @returns an Array of medication objects, that have the parameter `name` as name.
     */
    findMedicationByName: function (arrMeds, name) {
        let medWithName = arrMeds.filter(m => m.nome === name);
        return medWithName;
    },

    /**
     * Gets the therapies associated with `currentDay` and `currentWeekDay`.
     * 
     * @param {Array<Object>} therapies array with today's therapies.
     * @param {String} currentDate the current date.
     * @param {String} currentWeekDay the current day.
     * @returns a Promise with the resolved therapies.
     */
    getTodayTherapies: function (therapies, currentDate, currentWeekDay) {
        return new Promise(resolve => {
            var todayTherapies = [];
            for (const t of therapies) {
                // If it's a valid therapy
                if (ud.isSameOrAfterDate(currentDate, t.valido.inizio) && ud.isSameOrBeforeDate(currentDate, t.valido.fine)) {
                    if (t.frequenza.giorni.indexOf(currentWeekDay) !== -1) {
                        todayTherapies.push(t);
                    }
                }
            }
            resolve(todayTherapies);
        });
    },

    /**
     * If there are some medication objects in the DB, associated with the currentDate, then they
     * are taken in consideration when setting the property `assunto` for today's therapies.
     * 
     * @param {Array<Object>} todayT array with today's therapies. 
     * @param {Array<Object>} medicationsFromDB array with medication objects associated with currentDate.
     * @returns a Promise with the resolved therapies.
     */
    updateTakenMedicationsInTodayTherapiesFromDB: function (todayT, medicationsFromDB) {
        return new Promise(resolve => {
            for (const m of medicationsFromDB) {
                // For each object in medicationsFromDB {id: 'AAA', ora: '09:00', ...}, find the therapy with said id and change the 'assunto' property
                let index = todayT.findIndex(t => t.id === m.id);
                (todayT[index].dettaglioAssunzione.orario.find(o => o.ora === m.ora)).assunto = m.assunto;
            }

            resolve(todayT);
        });
    },

    /**
     * Checks whether a therapy has expired or not. If at leat one therapy has expired then it returns `true`.
     * 
     * @param {Array<Object>} therapies array with today's therapies. 
     * @param {String} currentDate the current date.
     * @returns a Boolean - `true` if at least one therapy has expired, `false` otherwise.
     */
    checkExpirationForAtLeastATherapy: function (therapies, currentDate) {
        for (const t of therapies) {
            // If one therapy's expired, retrieves all therapies (as an update)
            if (ud.isAfterDate(currentDate, t.valido.fine)) {
                return true;
            }
        }
        return false;
    },

    /**
     * Builds an unplanned medication's object.
     * 
     * @param {String} currentDate the current date (in the following format DD/MM/YYYY).
     * @param {String} nomeFarmaco name of the medication.
     * @param {String} oraAssunzione string with the time the medication was taken.
     * @param {Number} doseAssunzione the number of medication's units taken.
     * @param {String} tipoDose the type of medication's unit.
     * @returns an Object with the unplanned medication.
     */
    buildUnplannedMedication: function (currentDate, nomeFarmaco, oraAssunzione, doseAssunzione, tipoDose) {
        let medication = {
            assunto : true,
            data: currentDate,
            nome: nomeFarmaco,
            oraAssunzione: oraAssunzione,
            dose: parseInt(doseAssunzione),
            tipoDose: dialogs.getPluralFromWordAndQuantity(tipoDose, doseAssunzione)
        }
        return medication;
    },

    /**
     * Gets the time the medication was taken based on the `medication_time` object.
     * 
     * @param {Object} medication_time object with the time the user said he/she took the medication.
     * This might have two parameter `datetime` (if the user said an explicit time) or `now_time` (if
     * the user said 'adesso','ora',...). 
     * @returns a String with the parsed time.
     */
    parseMedicationTime: function (medication_time) {
        if (medication_time.datetime) {
            // Time specified by the user
            return ud.getFormattedDate(medication_time.datetime, ud.justTime);
        } else {
            // Current hour
            return ud.getFormattedDate(ud.getCurrentDateTime(), ud.justTime);
        }
    }
}

