/**
 * A module that wraps up all the methods related to the database.
 * @module firebase-db
 * @author Alessandra Semeraro
 */

var conn = require("./firebase-connector");

const db = conn.admin.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true };
db.settings(settings);

module.exports = {
    /**
     * Save a user's document to the firestore database.
     * 
     * @param {String} name first name from google.
     * @param {String} surname family name from google.
     * @param {String} email email from google.
     * @param {String} uid User ID from auth db.
     */
    saveUserInDB: function (name, surname, uemail, uid) {
        // References to collections
        var utentiRef = db.collection('utenti').doc(uid);

        utentiRef.set({
            nome: name,
            cognome: surname,
            email: uemail
        });
    },

    /**
     * Retrieves all the therapies related to a given user (identified by the param userId).
     * 
     * @param {String} userId UID that identifies the user in Firestore.
     * @returns a Promise with the resulting therapies.
     */
    getTherapiesFromUser: function (userId) {
        return new Promise(resolve => {
            let utentiTerapieRef = db.collection('utenti').doc(userId).collection('terapie');
            utentiTerapieRef.get()
            .then(snapshot => {
                // Array of objects
                let therapies = [];
                snapshot.forEach(doc => {
                    t = doc.data();
                    t.id = doc.id;
                    therapies.push(t);
                });
                return resolve(therapies);
            })
            .catch(err => {
                console.log('Error getting documents', err);
            });
        });
    },

    /**
     * Retrieves all the taken/not-taken medications in the DB based on the therapy ID and the date.
     * 
     * @param {String} userId UID that identifies the user in Firestore.
     * @param {String} therapyId the therapy ID.
     * @param {String} date the date under which the medications must be looked up.  
     * @returns a Promise with the resulting medications (or an empty array).
     */
    getMedicationsBasedOnTherapyIdAndDate: function (userId, therapyId, date) {
        return new Promise(resolve => {
            let utentiTerapieAssunzioniRef = db
                .collection('utenti')
                .doc(userId)
                .collection('terapie')
                .doc(therapyId)
                .collection('assunzioni')
                .where("data", "==", date);

            utentiTerapieAssunzioniRef.onSnapshot(querySnapshot => {
                // Array of objects
                let medications = [];
                querySnapshot.forEach(doc => {
                    m = doc.data();
                    medications.push(m);
                });
                resolve(medications);
            });
        });
    },

    /**
     * Gets the unread messages from the DB.
     * It also can get the messages under the current date.
     * 
     * @param {String} userId UID that identifies the user in Firestore.
     * @param {String} currentDate the current date.
     * @param {String} type the type of messages to download.
     * @returns a Promise with the resulting messages (or an empty array).
     */
    getDoctorMessages: function (userId, currentDate, type = 'NOT READ') {
        return new Promise(resolve => {
            var utentiMessaggiMedicoRef;
            switch (type) {
                case 'NOT READ':
                    utentiMessaggiMedicoRef = db
                        .collection('utenti')
                        .doc(userId)
                        .collection('messaggiMedico')
                        .where('confermaLettura', '==', false)
                        .orderBy('data', 'desc');
                    break;
                case 'TODAY':
                    utentiMessaggiMedicoRef = db
                        .collection('utenti')
                        .doc(userId)
                        .collection('messaggiMedico')
                        .where('data', '==', currentDate)
                        .orderBy('data', 'desc');
                    break;
                default:
                    resolve([]);
            }
            utentiMessaggiMedicoRef.onSnapshot(querySnapshot => {
                // Array of objects
                let messages = [];
                querySnapshot.forEach(doc => {
                    m = doc.data();
                    m.id = doc.id;
                    messages.push(m);
                });
                resolve(messages);
            });
        });
    },

    /**
     * Updates the messages read by the user, setting the property `confermaLettura` to true in the DB.
     * 
     * @param {String} userId UID that identifies the user in Firestore.
     * @param {Array<Object>} messages an array with messages as objects.
     * @returns a Promise when the operation has ended.
     */
    updateReadMessages: function (userId, messages) {
        return new Promise(resolve => {
            for (const m of messages) {
                let utentiMessaggiMedicoRef = db
                    .collection('utenti')
                    .doc(userId)
                    .collection('messaggiMedico')
                    .doc(m.id);
                utentiMessaggiMedicoRef.update({
                    confermaLettura: true
                });
            }
            resolve();
        });
    },

    /**
     * Saves the periodic check-up's results in the DB.
     * 
     * @param {String} userId UID that identifies the user in Firestore.
     * @param {String} currentDate the current date.
     * @param {String} healthStatus the user's health status.
     * @param {Array<Strings>} symptoms an array with all the symptoms accused by the user.
     * @returns a Promise when the write is done.
     */
    saveCheckUp: function (userId, currentDate, healthStatus, symptoms) {
        return new Promise(resolve => {
            let utentiCheckUpsRef = db
                .collection('utenti')
                .doc(userId)
                .collection('checkUp')
                .doc();
            utentiCheckUpsRef.set({
                data: currentDate,
                statoSalute: healthStatus,
                sintomi: symptoms
            });
            resolve();
        });
    },

    /**
     * Saves the symptoms in the DB.
     * 
     * @param {String} userId UID that identifies the user in Firestore.
     * @param {String} currentDate the current date.
     * @param {Array<Strings>} symptoms an array with all the symptoms accused by the user.
     * @returns a Promise when the write is done.
     */
    saveSymptoms: function (userId, currentDate, symptoms) {
        return new Promise(resolve => {
            let utentiSintomiRef = db
                .collection('utenti')
                .doc(userId)
                .collection('sintomiSegnalazioni')
                .doc();
            utentiSintomiRef.set({
                data: currentDate,
                sintomi: symptoms
            });
            resolve();
        });
    },

    /**
     * Saves the medications passed as a parameter in the DB.
     * 
     * @param {String} userId UID that identifies the user in Firestore. 
     * @param {Array<Object>} medications an array with the medication object to save in the DB.
     * @returns a Promise after the write is complite.
     */
    saveMedicationsTaken: function (userId, medications) {
        return new Promise(async resolve => {
            var batch = db.batch();
            for (const m of medications) {
                let utentiTerapieAssunzioniRef = db
                    .collection('utenti')
                    .doc(userId)
                    .collection('terapie')
                    .doc(m.id)
                    .collection('assunzioni')
                    .doc();
                m.assunto = true;
                batch.set(utentiTerapieAssunzioniRef, m);
            }
            return batch.commit().then(()=>{return resolve();});
        });
    },

    /**
     * Saves a single unplanned medication in the DB.
     * 
     * @param {String} userId UID that identifies the user in Firestore. 
     * @param {Object} medication unplanned medication object to save.
     */
    saveUnplannedMedication: function (userId, medication) {
        return new Promise(async resolve => {
            var batch = db.batch();
            let utentiAssunzioniNonProgrammateRef = db
                .collection('utenti')
                .doc(userId)
                .collection('assunzioniNonProgrammate')
                .doc();
            batch.set(utentiAssunzioniNonProgrammateRef, medication);
            return batch.commit().then(()=>{return resolve();});
        });
    }
}





