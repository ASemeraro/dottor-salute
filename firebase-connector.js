/**
 * Database connector.
 * @module firebase-connector
 * @author Alessandra Semeraro
 */

var admin = require("firebase-admin");
 
var serviceAccount = require("./configuration_files/firebase-private-key.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://your-firebase-project.firebaseio.com"
});

module.exports.admin = admin;
