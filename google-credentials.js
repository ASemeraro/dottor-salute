/**
 * Google credentials.
 * @module google-credentials
 * @author Alessandra Semeraro
 */

// Client ID issued by Google to the Action
// It can be retrieved while setting the account linking in the Actions Console
const actionsClientID = 'CLIENT-ID-ACTIONS-ON-GOOGLE';

/**
 * Gets the Client ID of the Action
 * @returns a String with the Client ID issued by Google to the Action
 */
function getActionsClientID() {
    return actionsClientID;
}

module.exports.getActionsClientID = getActionsClientID;