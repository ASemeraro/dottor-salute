# DOTTOR SALUTE (progetto di tesi)

Si tratta di un'Action per Google Assistant che, insieme al supporto di una piattaforma (non ancora realizzata), si occuperà di agire da intermediaro tra medico e paziente.

## Getting Started

Le istruzioni contenute nei seguenti documenti hanno lo scopo di permettere a chiunque di ottenere una copia del progetto e testarlo sul proprio account Google.
I documenti sono i seguenti:
* [Settare DialogFlow - Firebase - Actions on Google](https://gitlab.com/ASemeraro/dottor-salute/blob/master/tutorial/Settare%20DialogFlow%20-%20Firebase%20-%20Actions%20on%20Google.pdf)
* [Firestore Tutorial](https://gitlab.com/ASemeraro/dottor-salute/blob/master/tutorial/Firestore%20Tutorial.pdf)

PS: I documenti sono stati scritti da me, quindi consiglierei di affiancare a ciò che ho scritto anche la documentazione ufficiale di Google.

### Prerequisiti

* Aver creato un progetto in DialogFlow (e aver importato lo zip [dottor-salute.zip](https://gitlab.com/ASemeraro/dottor-salute/blob/master/dottor-salute.zip))
* Aver creato un progetto (associato) in Firebase

## Documentazione

La documentazione è contenuta nella cartella **out**. Una volta scaricata la cartella aprire il file **index.html**.
