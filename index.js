/**
 * Main file with the VUI's logic.
 * @module index
 * @author Alessandra Semeraro
 */

'use strict';

/**
 * Import the Dialogflow module and response creation dependencies
 * from the Actions on Google client library.
 */
const {
  dialogflow,
  SignIn,
  Suggestions,
} = require('actions-on-google');

/**
 * Array with input contexts that are safe to delete all at once.
 */
const CONTEXTS = [
  'DefaultWelcomeIntent-followup',
  'taken-meds-prompted',
  'awaiting-confirmation',
  'awaiting-time',
  'TakenMedications-whichOne-followup',
  'check-up-prompted',
  'check-up-ongoing',
  'StartCheckUpFlow-yes-yes-healthStatus-followup',
  'StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup',
  'unread-messages-prompted'
];

// Import the firebase-functions package for deployment
const functions = require('firebase-functions');
const format = require('string-format');

const ud = require('./utility-date');
const ut = require('./utility-therapies');
const um = require('./utility-messages');

const creds = require('./google-credentials');
const dialogs = require('./dialog-strings');
const userprofile = require('./google-oauth-api');
const conn = require('./firebase-connector');
const fd = require('./firebase-db');

/** Instantiate the Dialogflow client */
const app = dialogflow({
  clientId: creds.getActionsClientID(),
});

/**
 * This function substitutes the classic `conv.ask('Say something')`. In addition to
 * speaking the dialog passed as a parameter, this function saves said dialog in a 
 * session variable `conv.data.lastPrompt`, so that it can be used by the `RepeatIntent`
 * when the user asks the agent to repeat the last prompt.
 * 
 * @param {DialogflowConversation} conv conversation object 
 * @param {String} inputPrompt a string that contains the dialog the agent will speak
 */
function ask(conv, inputPrompt) {
  conv.data.lastPrompt = inputPrompt;
  conv.ask(inputPrompt);
}

app.intent('clean-storage-utility', conv => {
  conv.user.storage = {};
  conv.close('Storage pulito!');
});

app.intent('cancella_terapie', conv => {
  delete conv.user.storage.therapies;
  delete conv.user.storage.todayTherapies;
  conv.close('terapie cancellate!');
});

app.intent('Default Welcome Intent',
  /**
   * The Dafault Welcome Intent is matched at the start of the conversation.
   * It prompts the user with the sign-in flow, if the user has not been authenticated yet.
   * @function defaultWelcomeIntentHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv) => {
    conv.data.currentDate = ud.getCurrentDateTime();
    conv.data.currentSimpleDate = ud.getFormattedDate(conv.data.currentDate, ud.justDate);
    conv.data.currentWeekDay = ud.getFormattedDate(conv.data.currentDate, ud.justDay);
    // To check if the user is not logged in and the Account Linking has not been done yet
    if (!conv.user.storage.uid) {
      conv.data.fallbackCount = 0;
      ask(conv, dialogs.greetings_first_time);
      return conv.ask(new Suggestions('Sì', 'No'));
    } else {
      conv.data.fallbackCount = 0;
      // Delete Default Welcome Intent - followup context (so that the user doesn't have to go through
      // the sign in process if a 'yes' response is given)
      conv.contexts.delete('DefaultWelcomeIntent-followup');
      conv.contexts.set('authenticated', 99);

      if (ud.isSameOrAfterDate(conv.data.currentDate, conv.user.storage.lastAskedCheckUp, 1, ud.offsetTypeWeek)) {
        return ask(conv, format(dialogs.getRandomDialog(dialogs.simple_greetings), conv.user.storage.firstName) + ' ' + promptWithCheckUp(conv));
      }
      // If there's at least one therapy
      if (conv.user.storage.therapies && conv.user.storage.therapies.length > 0) {
        // If one of the therapies has expired
        // OR
        // If the timestamp of today's therapies comes before the current date, then check for new therapies
        if (ut.checkExpirationForAtLeastATherapy(conv.user.storage.therapies, conv.data.currentDate) || ud.isAfterDate(conv.data.currentDate, conv.user.storage.todayTherapiesTimestap)) {
          await updateTherapies('ALL', conv, conv.user.storage.uid, conv.data.currentDate, conv.data.currentWeekDay);
        }
        // Prompt with unread messages (if present) once per day
        // The user can always ask for unread messages and the relative Intent will take care of that
        if (!conv.user.storage.lastAskedUnreadMessages || ud.isAfterDate(conv.data.currentDate, conv.user.storage.lastAskedUnreadMessages)) {
          conv.data.unreadMessages = await um.checkUnreadMessages(conv.user.storage.uid);
          if (conv.data.unreadMessages) {
            conv.user.storage.lastAskedUnreadMessages = conv.data.currentDate;
            return promptWithUnreadMessages(conv, conv.data.unreadMessages);
          }
        }
        // Then just check for medications' remainders
        let medicationsRemaindersString = await promptWithRemainders(conv);
        if (medicationsRemaindersString)
          return ask(conv, format(dialogs.default_welcome_med_reminder, medicationsRemaindersString));
        else
          return ask(conv, format(dialogs.getRandomDialog(dialogs.greetings), conv.user.storage.firstName));

        //If there are no therapies in the local storage, check for new therapies
      } else {
        await updateTherapies('ALL', conv, conv.user.storage.uid, conv.data.currentDate, conv.data.currentWeekDay);
        // Always check for new messages
        if (!conv.user.storage.lastAskedUnreadMessages || ud.isAfterDate(conv.data.currentDate, conv.user.storage.lastAskedUnreadMessages)) {
          conv.data.unreadMessages = await um.checkUnreadMessages(conv.user.storage.uid);
          if (conv.data.unreadMessages) {
            return promptWithUnreadMessages(conv, conv.data.unreadMessages);
          }
        }
        let medicationsRemaindersString = await promptWithRemainders(conv);
        if (medicationsRemaindersString)
          return ask(conv, format(dialogs.default_welcome_med_reminder, medicationsRemaindersString));
        else
          return ask(conv, format(dialogs.getRandomDialog(dialogs.greetings), conv.user.storage.firstName));
      }
    }
  });

app.intent('ListenToMessages - ask',
  /**
   * ListenToMessages - ask Intent is matched when the user asks if there are new messages to listen to.
   * 
   * InputContext(s) : `authenticated`
   * 
   * OutputContext(s) : `none` or `unread-messages-prompted` (if the user has new messages)
   * @function listenToMessagesAskHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv) => {
    cleanContexts(conv);
    conv.data.unreadMessages = await um.checkUnreadMessages(conv.user.storage.uid);
    if (conv.data.unreadMessages) {
      return promptWithUnreadMessages(conv, conv.data.unreadMessages);
    } else {
      return ask(conv, dialogs.no_messages);
    }
  });

app.intent('ListenToMessages',
  /**
   * ListenToMessages - Intent is matched when the user expresses his/her will to listen to one or all
   * messages.
   * 
   * InputContext(s) : `unread-messages-prompted`
   * 
   * OutputContext(s) : `none`
   * @function listenToMessagesHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv, { ordinal }) => {
    // Clean all possible incoming contexts (except 'authenticated')
    cleanContexts(conv);
    let unreadMessages = conv.data.unreadMessages;
    switch (ordinal) {
      case 'ultimo':
        // Ascending order (from the most recent to the least recent)
        await fd.updateReadMessages(conv.user.storage.uid, [unreadMessages[unreadMessages.length - 1]]);
        ask(conv, format(dialogs.just_last_message, dialogs.getFormattedSSMLMessages([unreadMessages[unreadMessages.length - 1]])));
        cleanContexts(conv);
        return conv.ask(dialogs.that_was_the_last_message);

      case 'tutto':
        await fd.updateReadMessages(conv.user.storage.uid, unreadMessages);
        ask(conv, format(dialogs.list_of_all_messages, dialogs.getFormattedSSMLMessages(unreadMessages)));
        cleanContexts(conv);
        return conv.ask(dialogs.that_was_the_last_message);

      case 'nessuno':
        cleanContexts(conv);
        return conv.ask(dialogs.none_of_the_messages_are_selected);

      default:
        if (conv.data.fallbackCount > 2) {
          return conv.close(dialogs.fallback_listen_to_messages[conv.data.fallbackCount]);
        } else {
          conv.data.fallbackCount++;
          return ask(conv, dialogs.fallback_listen_to_messages[conv.data.fallbackCount - 1]);
        }
    }
  });

app.intent('GetTherapies',
  /**
   * GetTherapies Intent is matched when the user asks for today's therapies, or well the ones that have
   * not yet been taken.
   * 
   * InputContext(s) : `authenticated`
   * 
   * OutputContext(s) : `none`
   * @function getTherapiesHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv) => {
    // Clean all possible incoming contexts (except 'authenticated')
    cleanContexts(conv);
    // If there are no therapies in the storage (for example: first time user, no therapies in the database)
    if (!conv.user.storage.therapies || (conv.user.storage.therapies && conv.user.storage.therapies.length === 0)) {
      await updateTherapies('ALL', conv, conv.user.storage.uid, conv.data.currentDate, conv.data.currentWeekDay);
      return await promptWithTodaysTherapies(conv);
    } else {
      await updateTherapies('ALL', conv, conv.user.storage.uid, conv.data.currentDate, conv.data.currentWeekDay);
      return await promptWithTodaysTherapies(conv);
    }
  });

//****************************** REGISTER SINGLE MEDICATION *******************************

app.intent('RegisterSingleMedication',
  /**
   * RegisterSingleMedication Intent is matched when the user says he/she has taken a particular medication,
   * which can be planned (in today's therapies) or unplanned (not in today's therapies).
   * 
   * InputContext(s) : `none`
   * 
   * OutputContext(s) : `awaiting-name` or `awaiting-time` or `awaiting-quantity` or `none`
   * @function registerSingleMedicationHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv, { medication, medication_time, number, medication_type }) => {
    if (!conv.user.storage.uid) {
      // This starts the Sign In flow
      return conv.ask(new SignIn(`Per usare quest'applicazione,`));
    }
    conv.data.currentDate = ud.getCurrentDateTime();
    conv.data.currentSimpleDate = ud.getFormattedDate(conv.data.currentDate, ud.justDate);
    conv.data.currentWeekDay = ud.getFormattedDate(conv.data.currentDate, ud.justDay);
    // I do this if the action is not started through standard methods (I'm talking about the DefaultWelcomeIntent, where I do all the operations
    // at the beginning of the conversation)
    await updateTherapies('ALL', conv, conv.user.storage.uid, conv.data.currentDate, conv.data.currentWeekDay);
    
    conv.contexts.set('authenticated', 99);
    cleanContexts(conv);

    if (!medication) {
      conv.data.fallbackCount = 0;
      conv.contexts.set('awaiting-name', 4, { number: number, medication_type: medication_type });
      return ask(conv, dialogs.register_single_medication_ask_for_name);
    } else if (!medication_time) {
      conv.data.fallbackCount = 0;
      conv.contexts.set('awaiting-time', 4, { medication: medication, number: number, medication_type: medication_type });
      return ask(conv, format(dialogs.register_single_medication_ask_for_time, medication));
    }
    // Now there's the pair medication's name (e.g. "Maxalt") and time (e.g. "9:00")
    let time = ut.parseMedicationTime(medication_time);
    console.log("TIME" + time);
    // Retrieve medication with 'ora' property that comes before (or after if before there's nothing) the time provided by the user
    let medFound = ut.findMedicationByNameAndTime(
      ut.getTodaysNotTakenMeds(conv.user.storage.todayTherapies, conv.data.currentSimpleDate),
      medication,
      time
    );
    if (medFound) {
      medFound.oraAssunzione = time;
      console.log(`medfound: ${JSON.stringify(medFound)}`);
      // End
      return promptWithSinglePlannedMedicationConfirmation(conv, medFound);
    } else {
      if (!number && !medication_type) {
        conv.data.fallbackCount = 0;
        conv.contexts.set('awaiting-quantity', 4, { medication: medication, time: time });
        return ask(conv, format(dialogs.register_single_medication_ask_for_quantity, medication));
      } else {
        // End
        return promptWithSingleUnplannedMedicationConfirmation(conv, medication, time, number, medication_type);
      }
    }
  });

app.intent('RegisterSingleMedication - fillName',
  /**
   * RegisterSingleMedication - fillName Intent is matched when the user provides the name of the medication
   * he/she took.
   * 
   * InputContext(s) : `awaiting-name`
   * 
   * OutputContext(s) : `awaiting-time`
   * @function registerSingleMedicationFillNameHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  (conv, { medication }) => {
    const context = conv.contexts.get('awaiting-name');
    const number = context.parameters.number;
    const medication_type = context.parameters.medication_type;

    if (!medication) {
      if (conv.data.fallbackCount > 2) {
        return conv.close(dialogs.reprompt_register_single_medication_name[conv.data.fallbackCount]);
      } else {
        conv.data.fallbackCount++;
        return ask(conv, dialogs.reprompt_register_single_medication_name[conv.data.fallbackCount - 1]);
      }
    } else {
      conv.contexts.delete('awaiting-name');
      conv.data.fallbackCount = 0;
      conv.contexts.set('awaiting-time', 4, { medication: medication, number: number, medication_type: medication_type });
      return ask(conv, format(dialogs.register_single_medication_ask_for_time, medication));
    }
  });

app.intent('RegisterSingleMedication - fillTime',
  /**
   * RegisterSingleMedication - fillTime Intent is matched when the user provides the time at which
   * he/she took the medication.
   * 
   * InputContext(s) : `awaiting-time`
   * 
   * OutputContext(s) : `awaiting-quantity`
   * @function registerSingleMedicationFillTimeHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  (conv, { medication_time }) => {
    const context = conv.contexts.get('awaiting-time')
    const medication = context.parameters.medication;
    const number = context.parameters.number;
    const medication_type = context.parameters.medication_type;

    if (!medication_time) {
      if (conv.data.fallbackCount > 2) {
        return conv.close(dialogs.reprompt_register_single_medication_time[conv.data.fallbackCount]);
      } else {
        conv.data.fallbackCount++;
        return ask(conv, format(dialogs.reprompt_register_single_medication_time[conv.data.fallbackCount - 1], medication));
      }
    } else {
      conv.contexts.delete('awaiting-time');
      // Now there's the pair medication's name (e.g. "Maxalt") and time (e.g. "9:00")
      let time = ut.parseMedicationTime(medication_time);
      // Retrieve medication with 'ora' property that comes before the time provided by the user
      let medFound = ut.findMedicationByNameAndTime(
        ut.getTodaysNotTakenMeds(conv.user.storage.todayTherapies, conv.data.currentSimpleDate),
        medication,
        time
      );
      if (medFound) {
        medFound.oraAssunzione = time;
        return promptWithSinglePlannedMedicationConfirmation(conv, medFound);
      } else {
        if (!number && !medication_type) {
          conv.data.fallbackCount = 0;
          conv.contexts.set('awaiting-quantity', 4, { medication: medication, time: time });
          return ask(conv, format(dialogs.register_single_medication_ask_for_quantity, medication));
        } else {
          // Got everything to register the unplanned medication
          return promptWithSingleUnplannedMedicationConfirmation(conv, medication, time, number, medication_type);
        }
      }
    }
  });


app.intent('RegisterSingleMedication - fillQuantity',
  /**
   * RegisterSingleMedication - fillQuantity Intent is matched when the user provides the quantity of
   * the medication he/she took.
   * 
   * InputContext(s) : `awaiting-quantity`
   * 
   * OutputContext(s) : `none`
   * @function registerSingleMedicationFillQuantityHandler
   * @param {DialogflowConversation} conv conversation object.  
   */
  (conv, { number, medication_type }) => {
    const context = conv.contexts.get('awaiting-quantity')
    const medication = context.parameters.medication;
    const time = context.parameters.time;
    if (!number || !medication_type) {
      if (conv.data.fallbackCount > 2) {
        return conv.close(dialogs.reprompt_register_single_medication_quantity[conv.data.fallbackCount]);
      } else {
        conv.data.fallbackCount++;
        return ask(conv, format(dialogs.reprompt_register_single_medication_quantity[conv.data.fallbackCount - 1], medication));
      }
    } else {
      // Got everything to register the unplanned medication
      conv.contexts.delete('awaiting-quantity');
      return promptWithSingleUnplannedMedicationConfirmation(conv, medication, time, number, medication_type);
    }
  });

//******************************** PROMPTED MEDICATIONS *********************************

app.intent('TakenMedications - yes',
  /**
   * TakenMedications - yes is matched when the user says he/she has taken ALL the late medications in
   * time, but just forgot to tell Miranda so.
   * 
   * The handler sets the value of `assunto` for each medication in todayTherapies to true. It also saves
   * the medications in the DB.
   * 
   * InputContext(s) : `taken-meds-prompted`
   * 
   * OutputContext(s) : `none`
   * @function takenMedicationsYesHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  async (conv) => {
    const context = conv.contexts.get('taken-meds-prompted');
    const medicationsIDs = context.parameters.medicationsIDs;
    conv.user.storage.todayTherapies = ut.takeMedicationsById(conv.user.storage.todayTherapies, medicationsIDs);
    for (const m of medicationsIDs) {
      m.assunto = true;
    }
    await fd.saveMedicationsTaken(conv.user.storage.uid, medicationsIDs);
    cleanContexts(conv);
    ask(conv, dialogs.taken_all_therapies);
  });


app.intent('TakenMedications - no',
  /**
   * TakenMedications - no is matched when the user says he/she has taken NONE OF the late medications in
   * time.
   * 
   * The handler sets the value of `assunto` for each medication in todayTherapies to 'non assunto'. It also saves
   * the medication in the DB.
   * 
   * InputContext(s) : `taken-meds-prompted`
   * 
   * OutputContext(s) : `none`
   * @function takenMedicationsNoHandler
   * @param {DialogflowConversation} conv conversation object.  
   */
  async (conv) => {
    const context = conv.contexts.get('taken-meds-prompted');
    const medicationsIDs = context.parameters.medicationsIDs;
    conv.user.storage.todayTherapies = ut.refuseMedicationsById(conv.user.storage.todayTherapies, medicationsIDs);
    for (const m of medicationsIDs) {
      m.assunto = 'non assunto';
    }
    await fd.saveMedicationsTaken(conv.user.storage.uid, medicationsIDs);
    cleanContexts(conv);
    ask(conv, dialogs.taken_none_of_the_therapies);
  });

app.intent('TakenMedications - notYet',
  /**
   * TakenMedications - no is matched when the user says he/she has not yet taken the medications.
   * 
   * InputContext(s) : `taken-meds-prompted`
   * 
   * OutputContext(s) : `none`
   * @function takenMedicationsNotYetHandler
   * @param {DialogflowConversation} conv conversation object.   
   */
  conv => {
    cleanContexts(conv);
    ask(conv, dialogs.not_yet_taken_therapies);
  });

app.intent('TakenMedications - whichOne',
  /**
   * TakenMedications - whichOne is matched when the user says:
   * - The number of the medication (e.g. 'la prima e l'ultima che hai detto');
   * - JUST the name of the medication (e.g 'il maxalt e il totalip'), which will prompt a time confirmation;
   * - The name and the time of the medication (e.g 'il maxalt alle 10:00').
   * 
   * ATTENTION: Here the fallback is handled inside the intent handler (anonymous asyncronous function).
   * One of the entities accepted for this Intent is `sys.any`, which lets us match this Intent every single
   * time we say something that is not present in the training phrases of TakenMedications - yes/no/notYet.
   * 
   * InputContext(s) : `taken-meds-prompted`
   * 
   * OutputContext(s) : `none` or 
   *                    `TakenMedications-whichOne-followup`, `awaiting-confirmation` or
   *                    `TakenMedications-whichOne-followup`, `awaiting-confirmation`, `awaiting-time`
   * @function takenMedicationsWhichOneHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  async (conv, { ordinal, medications, taken_medications_with_time }) => {
    // If none of the accepted input is given
    if (ordinal.length === 0 && medications.length === 0 && taken_medications_with_time.length === 0) {
      if (conv.data.fallbackCount > 2) {
        return conv.close(dialogs.reprompt_not_taken_medications[conv.data.fallbackCount]);
      } else {
        conv.data.fallbackCount++;
        let medicationsRemaindersString = await promptWithRemainders(conv);
        return ask(conv, format(dialogs.reprompt_not_taken_medications[conv.data.fallbackCount - 1], medicationsRemaindersString));
      }
    }
    const context = conv.contexts.get('taken-meds-prompted');
    // Array of prompted medications
    const medicationsIDs = context.parameters.medicationsIDs;
    // Will contain the single values in `medications`
    let setNameConfirmMedications = new Set();
    // Will contain the total of the medications indicated (by ordinal, name or name/time) by the user
    let arrConfirmMedications = [];
    // This array will be updated alongside arrConfirmMedications
    let notYetConfirmedMedications;
    if (ordinal) {
      // Delete duplicates
      ordinal = Array.from(new Set(ordinal));
      if (ordinal.indexOf('tutto') !== -1) {
        // All medications were taken
        cleanContexts(conv);
        conv.user.storage.todayTherapies = ut.takeMedicationsById(conv.user.storage.todayTherapies, medicationsIDs);
        return ask(conv, dialogs.taken_all_therapies);
      } else if (ordinal.indexOf('nessuno') !== -1) {
        // No medication was taken
        cleanContexts(conv);
        conv.user.storage.todayTherapies = ut.refuseMedicationsById(conv.user.storage.todayTherapies, medicationsIDs);
        return ask(conv, dialogs.taken_none_of_the_therapies);
      } else {
        // Do a subtraction between all meds and the confirmed meds (so that I can operate on the not-yet-confirmed medications)
        notYetConfirmedMedications = ut.getMedicationsFromFirstSet(new Set(medicationsIDs), new Set(arrConfirmMedications));
        // Remember you have an array with the medicationsIDs [{id:'aaa', ..., ora:'9:00'}, {id:'aaa', ..., ora:'14:00'}]
        for (const n of ordinal) {
          if (parseInt(n) > notYetConfirmedMedications.length) {
            continue;
          }
          switch (n) {
            case 'ultimo':
              // Push in the empty array the last element of the medicationsIDs array
              arrConfirmMedications.push(notYetConfirmedMedications[notYetConfirmedMedications.length - 1]);
              break;
            case '1': case '2': case '3': case '4': case '5':
            case '6': case '7': case '8': case '9': case '10':
              arrConfirmMedications.push(notYetConfirmedMedications[parseInt(n) - 1]);
              break;
          }
        }
      }
    }
    if (taken_medications_with_time) {
      // Do a subtraction between all meds and the confirmed meds (so that I can operate on the not yet confirmed medications)
      notYetConfirmedMedications = ut.getMedicationsFromFirstSet(new Set(medicationsIDs), new Set(arrConfirmMedications));
      for (const tm of Array.from(new Set(taken_medications_with_time))) {
        // taken_medications_with_time is a composite entity (medication, medication_time)
        // Here we get the time (e.g. 9:30)
        let time = ut.parseMedicationTime(tm.medication_time);
        // Every medication with the name given by the user and the closest time
        let medFound = ut.findMedicationByNameAndTime(notYetConfirmedMedications, tm.medication, time); //{object1}
        if (medFound) {
          medFound.oraAssunzione = time;
          arrConfirmMedications.push(medFound);
        }
      }
    }
    if (medications) {
      // Do a subtraction between all meds and the confirmed meds (so that I can operate, for the doubles, on the not yet confirmed medications)
      notYetConfirmedMedications = ut.getMedicationsFromFirstSet(new Set(medicationsIDs), new Set(arrConfirmMedications));
      for (const mName of Array.from(new Set(medications))) {
        // Every medication with the name given by the user
        let arrMedsWithmName = ut.findMedicationByName(notYetConfirmedMedications, mName); //[{object1}, {object2}]
        if (arrMedsWithmName.length > 0) {
          setNameConfirmMedications.add(mName);
        }
      }
    }
    conv.contexts.delete('taken-meds-prompted');
    conv.contexts.set('TakenMedications-whichOne-followup', 4);
    conv.contexts.set('awaiting-confirmation', 4, { confirmedMedications: arrConfirmMedications, totalMedicationsIDs: medicationsIDs });
    // Ask for the correct time
    if (setNameConfirmMedications.size > 0) {
      // From here, the conversation moves to TakenMedications - whichOne - confirmTime Intent
      conv.data.fallbackCount = 0;
      // This will help iterate through the medications' names provided by the user, so that he/she can
      // confirm a time.
      conv.data.repromptConfirmationMedsTime = 0;
      conv.contexts.set('awaiting-time', 4, { listOfMedNames: Array.from(setNameConfirmMedications) });
      let firstMedName = Array.from(setNameConfirmMedications)[conv.data.repromptConfirmationMedsTime];
      conv.data.repromptConfirmationMedsTime++;
      // Gets all the hours associated with a particular medication's name and prompts the user so that
      // he/she can specify when he/she took said medication.
      return ask(conv, format(dialogs.taken_list_therapies_confirmation_time, firstMedName, dialogs.getStringFromArrayDisjunction(ut.getTimeFromMedicationObjects(notYetConfirmedMedications.filter(t => t.nome === firstMedName)))));
    } else {
      conv.data.fallbackCount = 0;
      // Ask for confirmation
      return await promptWithMedicationsConfirmation(conv, arrConfirmMedications, medicationsIDs);
    }
  });

app.intent('TakenMedications - whichOne - confirmTime',
  /**
   * TakenMedications - whichOne - confirmTime is matched when the user provides the time at which he/she
   * took the medication prompted by Miranda.
   * 
   * ATTENTION: Here the fallback is handled inside the intent handler (anonymous asyncronous function).
   * One of the entities accepted for this Intent is `sys.any`, which lets us match this Intent every single
   * time we say something that is not present in the training phrases of TakenMedications - whichOne - yes/no.
   * 
   * InputContext(s) : `TakenMedications-whichOne-followup`, `awaiting-confirmation`, `awaiting-time`
   * 
   * OutputContext(s) : `TakenMedications-whichOne-followup`, `awaiting-confirmation`, `awaiting-time`
   *                    (untill the last name in listOfMedNames is confirmed) or
   *                    `TakenMedications-whichOne-followup`, `awaiting-confirmation`
   * @function takenMedicationsWhichOneConfirmTimeHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  async (conv, { medication_time, ordinal }) => {
    if (medication_time.length === 0 && ordinal.length === 0) {
      if (conv.data.fallbackCount > 2) {
        return conv.close(dialogs.fallback_plus_replay[conv.data.fallbackCount]);
      } else {
        conv.data.fallbackCount++;
        return conv.ask(format(dialogs.fallback_plus_replay[conv.data.fallbackCount - 1], conv.data.lastPrompt));
      }
    }
    const context1 = conv.contexts.get('awaiting-confirmation');
    const context2 = conv.contexts.get('awaiting-time');
    // Always present
    let totalMedicationsIDs = context1.parameters.totalMedicationsIDs;
    let listOfMedNames = context2.parameters.listOfMedNames;
    // Could be empty, if no medication has been confirmed in the previous Intent
    let confirmedMedications = context1.parameters.confirmedMedications;
    let notYetConfirmedMedications;
    // Let's parse the answer from the user (e.g. ['date1', 'date2'] or ['entrambi'])
    if (ordinal) {
      // Delete duplicates
      ordinal = Array.from(new Set(ordinal));
      // Do a subtraction between all meds and the confirmed meds (so that I can operate, for the doubles, on the not yet confirmed medications)
      notYetConfirmedMedications = ut.getMedicationsFromFirstSet(new Set(totalMedicationsIDs), new Set(confirmedMedications));
      // Find all the medications with the name prompted by Miranda
      let medications = ut.findMedicationByName(notYetConfirmedMedications, listOfMedNames[conv.data.repromptConfirmationMedsTime - 1]);
      if (ordinal.indexOf('tutto') !== -1) {
        // All medications were taken
        for (const m of medications) {
          confirmedMedications.push(m);
        }
      } else if (ordinal.indexOf('nessuno') !== -1) {
        // Do nothing
      } else {
        // Remember you have an array with the medicationsIDs [{id:'aaa', ..., ora:'9:00'}, {id:'aaa', ..., ora:'14:00'}]
        for (const n of ordinal) {
          if (parseInt(n) > medications.length) {
            continue;
          }
          switch (n) {
            case 'ultimo':
              // Push in the empty array the last element of the medicationsIDs array
              confirmedMedications.push(medications[medications.length - 1]);
              break;
            case '1': case '2': case '3': case '4': case '5':
            case '6': case '7': case '8': case '9': case '10':
              confirmedMedications.push(medications[parseInt(n) - 1]);
              break;
          }
        }
      }
    }
    if (medication_time) {
      // Do a subtraction between all meds and the confirmed meds (so that I can operate, for the doubles, on the not yet confirmed medications)
      notYetConfirmedMedications = ut.getMedicationsFromFirstSet(new Set(totalMedicationsIDs), new Set(confirmedMedications));
      // Delete duplicates
      medication_time = Array.from(new Set(medication_time));
      for (const mt of medication_time) {
        let h = ut.parseMedicationTime(mt);
        let medication = ut.findMedicationByNameAndTime(notYetConfirmedMedications, listOfMedNames[conv.data.repromptConfirmationMedsTime - 1], h);
        // It might be differrent from the time scheduled for the medication
        medication.oraAssunzione = h;
        confirmedMedications.push(medication);
      }
    }
    // Reprompt with next medication's name, if there's such a thing
    conv.contexts.set('awaiting-confirmation', 4, { confirmedMedications: confirmedMedications, totalMedicationsIDs: totalMedicationsIDs });
    if (conv.data.repromptConfirmationMedsTime < listOfMedNames.length) {
      // Directs the conversation to the same intent
      conv.data.fallbackCount = 0;
      conv.contexts.set('awaiting-time', 4, { listOfMedNames: listOfMedNames });
      let medName = Array.from(listOfMedNames)[conv.data.repromptConfirmationMedsTime];
      conv.data.repromptConfirmationMedsTime++;
      return ask(conv, format(dialogs.taken_list_therapies_confirmation_time, medName, dialogs.getStringFromArrayDisjunction(ut.getTimeFromMedicationObjects(notYetConfirmedMedications.filter(t => t.nome === medName)))));
    } else {
      // Reprompt ended
      conv.contexts.delete('awaiting-time');
      // Ask for confirmation
      conv.data.fallbackCount = 0;
      return await promptWithMedicationsConfirmation(conv, confirmedMedications, totalMedicationsIDs);
    }
  });

app.intent('TakenMedications - whichOne - no',
  /**
   * TakenMedications - whichOne - no is matched when the user says no to the confirmation.
   * It moves the conversation back to step one, so to say. Basically, it prompts the user with the
   * list of late medications.
   * 
   * InputContext(s) : `TakenMedications-whichOne-followup`, `awaiting-confirmation`
   * 
   * OutputContext(s) : `taken-meds-prompted`
   * @function takenMedicationsWhichOneNoHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv) => {
    cleanContexts(conv);
    let medicationsRemaindersString = await promptWithRemainders(conv);
    return ask(conv, format(dialogs.reprompt_med_reminder, medicationsRemaindersString));
  });

app.intent('TakenMedications - whichOne - yes',
  /**
   * TakenMedications - whichOne - yes is matched when the user accepts the confirmation.
   * 
   * The handler sets the value of `assunto` for each medication in todayTherapies to true. It also saves
   * the medications in the DB.
   * 
   * InputContext(s) : `TakenMedications-whichOne-followup`, `awaiting-confirmation`
   * 
   * OutputContext(s) : `none`
   * @function takenMedicationsWhichOneYesHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv) => {
    const context = conv.contexts.get('awaiting-confirmation');
    const confirmedMedications = context.parameters.confirmedMedications;
    const atLeastOneLate = context.parameters.atLeastOneLate;
    conv.user.storage.todayTherapies = ut.takeMedicationsById(conv.user.storage.todayTherapies, confirmedMedications);
    for (const m of confirmedMedications) {
      m.assunto = true;
    }
    await fd.saveMedicationsTaken(conv.user.storage.uid, confirmedMedications);
    cleanContexts(conv);
    if (!atLeastOneLate)
      ask(conv, dialogs.taken_all_therapies);
    else
      ask(conv, dialogs.taken_therapies_late);
  });

//********************************* SIGN-IN SECTION **********************************

app.intent('Default Welcome Intent - yes',
  /**
   * Default Welcome Intent - yes Intent is triggered only if the user said yes to the `dialogs.greetings_first_time` message.
   * 
   * InputContext(s) : `DefaultWelcomeIntent-followup`
   * 
   * OutputContext(s) : `none`
   * 
   * GeneratedEvent(s) : `actions_intent_SIGN_IN`
   * @function defaultWelcomeIntentYesHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    conv.data.fallbackCount = 0;
    // This starts the Sign In flow
    conv.ask(new SignIn(`Per usare quest'applicazione,`));
  });

app.intent('actions_intent_SIGN_IN',
  /**
   * actions_intent_SIGN_IN Intent is triggered only at the end of a Sign In flow, by the `actions_intent_SIGN_IN`
   * event.
   * 
   * Event(s) : `actions_intent_SIGN_IN`
   * 
   * InputContext(s) : `none`
   * 
   * OutputContext(s) : `authenticated`, `check-up-prompted` or `none`
   * @function signInHandler
   * @param {DialogflowConversation} conv conversation object.
   * @param {*} params parameters stored in the event.
   * @param {ApiClientObjectMap<any>} signin a map with the sign in results.
   */
  (conv, params, signin) => {
    if (signin.status !== 'OK') {
      return conv.close(dialogs.sign_in_not_ok);
    } else {
      if (!conv.request.user.accessToken) {
        return conv.close(dialogs.sign_in_move_to_screen);
      }
      return userprofile
        .getUserProfile(conv.request.user.accessToken)
        .then(body => {
          // Save user's profile info to storage
          conv.user.storage.email = body.email;
          conv.user.storage.firstName = body.given_name;
          conv.user.storage.lastName = body.family_name;

          return conn.admin.auth().getUserByEmail(body.email);
        })
        .then(userRecord => {
          // Save the user's uid to the storage
          // This is a mandatory step for quering correctly the firebase firestore
          conv.user.storage.uid = userRecord.toJSON().uid;
          // fd.saveUserInDB(conv.user.storage.firstName, conv.user.storage.lastName, conv.user.storage.email, conv.user.storage.uid);
          ask(conv, format(
            dialogs.sign_in_ok,
            dialogs.getRandomDialog(dialogs.confirmation),
            conv.user.storage.firstName)
          );
          // The first thing that Miranda asks is a general check-up
          ask(conv, promptWithCheckUp(conv));
          conv.user.storage.accessToken = conv.request.user.accessToken;
          conv.contexts.set('authenticated', 99);
          return console.log('User authenticated');
        })
        .catch(err => {
          if (err.code !== 'auth/user-not-found') {
            // Clean the storage, that has been populated with the user's email, given_name and family_name
            // This data is useless if the user is non the DB, beacause he/she cannot use the application
            conv.user.storage = {};
            conv.close(dialogs.uncaught_problem);
            throw err;
          } else {
            console.error('User not found ' + err);
            conv.user.storage = {};
            conv.close(dialogs.user_not_fount);
          }
        });
    }
  });

//***************************** HEALTH STATUS / CHECK-UP *******************************

app.intent('RegisterHealthStatus',
  /**
   * RegisterHealthStatus Intent is matched when the user expresses some kind of discomfort or when
   * he/she declares his/her wellbeing.
   * 
   * InputContext(s) : `authenticated`
   * 
   * OutputContext(s) : `none` or `StartCheckUpFlow-yes-yes-healthStatus-followup`
   * @function registerHealthStatusHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  (conv, { health_status, health_status_original }) => {
    // Clean all possible incoming contexts excepts 'authenticated'
    cleanContexts(conv);
    conv.data.fallbackCount = 0;

    // Basically, if the patient says he/she is well, then Miranda is happy to hear that
    if (health_status && health_status.toLowerCase() === 'bene') {
      ask(conv, dialogs.register_health_state_good);
    } else {
      // Set OutputContext
      conv.contexts.set('StartCheckUpFlow-yes-yes-healthStatus-followup', 4);
      ask(conv, dialogs.check_up_step2_bad);
    }
  });

app.intent('StartCheckUpFlow - yes',
  /**
   * StartCheckUpFlow - yes Intent is matched when the user accepts to undergo a quick routine check-up.
   * 
   * InputContext(s) : `check-up-prompted`
   * 
   * OutputContext(s) : `StartCheckUpFlow-yes-followup`
   * @function startCheckUpFlowYesHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    conv.data.fallbackCount = 0;
    ask(conv, format(dialogs.check_up_consent_yes, dialogs.getRandomDialog(dialogs.confirmation)));
  });

app.intent('StartCheckUpFlow - yes - yes',
  /**
   * StartCheckUpFlow - yes - yes Intent is matched when the user confirms his/her consent to undergo a quick
   * routine check-up.
   * 
   * InputContext(s) : `StartCheckUpFlow-yes-followup`
   * 
   * OutputContext(s) : `StartCheckUpFlow-yes-yes-followup`, `check-up-ongoing`
   * @function startCheckUpFlowYesYesHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    conv.data.fallbackCount = 0;
    // This context will contain data to save to the database
    conv.contexts.set('check-up-ongoing', 40);
    ask(conv, format(dialogs.check_up_step1, dialogs.getRandomDialog(dialogs.confirmation)));
  });

app.intent('StartCheckUpFlow - yes - yes - healthStatus',
  /**
   * StartCheckUpFlow - yes - yes - healthStatus Intent is matched when the user states his/her health status,
   * which might be good or bad.
   * 
   * InputContext(s) : `StartCheckUpFlow-yes-yes-followup`
   * 
   * OutputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-followup`, `check-up-ongoing`
   * @function startCheckUpFlowYesYesHealthStatusHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  (conv, { health_status, health_status_original }) => {
    conv.data.fallbackCount = 0;
    conv.contexts.set('check-up-ongoing', 40, { health_status_original: health_status_original });
    // Basically, if the patient says he/she is well, then Miranda asks whether there've been symptoms
    // in the past few days
    if (health_status && health_status.toLowerCase() === 'bene') {
      ask(conv, dialogs.check_up_step2_good);
    } else {
      ask(conv, dialogs.check_up_step2_bad);
    }
  });

app.intent('StartCheckUpFlow - yes - yes - healthStatus - symptoms',
  /**
   * StartCheckUpFlow - yes - yes - healthStatus - symptoms Intent is matched when the user lists his/her symptoms.
   * `hurt_body_parts_original` is an array that contains a particular kind of symptoms, which are strictly related
   * to body parts and are accused in said body parts.
   * 
   * In this handler, the health status is saved to the database (if there are no symptoms).
   * 
   * ATTENTION: This Intent is used also after RegisterHealthStatus Intent. What is absent in that case is
   * the context `check-up-ongoing`.
   * 
   * InputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-followup`
   * 
   * OutputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup`, `check-up-ongoing`
   * @function startCheckUpFlowYesYesHealthStatusSymptomsHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  async (conv, { symptoms, hurt_body_parts, hurt_body_parts_original }) => {
    conv.data.fallbackCount = 0;
    conv.data.reprompt = 0;
    // Delete duplicates
    symptoms = Array.from(new Set(symptoms));
    hurt_body_parts_original = Array.from(new Set(hurt_body_parts_original));
    // Merge symptoms and hurt body parts
    let totSymptoms = symptoms.concat(hurt_body_parts_original);

    // This might be undefined if this Intent is matched after RegisterHealthStatus Intent
    const context = conv.contexts.get('check-up-ongoing');
    let health_status_original;
    if (context) {
      health_status_original = context.parameters.health_status_original;
    }

    if (totSymptoms.length > 0) {
      conv.contexts.set('StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup', 4);
      if (context) {
        // Save the list of symptoms (merged) in the context.
        // Said list will be saved in the DB at the end of the check-up.
        conv.contexts.set('check-up-ongoing', 40, { health_status_original: health_status_original, totSymptoms: totSymptoms });
      } else {
        // If this Intent is matched after RegisterHealthStatus Intent.
        // This will be saved to the DB after the user confirms the symptoms.
        conv.data.symptomsRegistration = totSymptoms;
      }
      ask(conv, format(dialogs.check_up_step2_confirmation, dialogs.getStringFromArray(totSymptoms)));
    } else {
      // If the patient is well, end this portion of the conversation and ask for something else
      cleanContexts(conv);
      if (context)
        await fd.saveCheckUp(conv.user.storage.uid, conv.data.currentSimpleDate, health_status_original, totSymptoms);
      ask(conv, dialogs.check_up_step3_no_symptoms);
    }
  });

app.intent('StartCheckUpFlow - yes - yes - healthStatus - symptoms - yes',
  /**
   * StartCheckUpFlow - yes - yes - healthStatus - symptoms - yes Intent is matched when the user adds some
   * symptoms to the list in `check-up-ongoing` context or when he/she says there's something else to add
   * at said list. Until the user doesn't provide at least one symptom or a negation, there will be a
   * reprompt (up to 3 times).
   * `hurt_body_parts_original` is an array that contains a special symptoms, which are strictly related to body
   * parts and are accused in said body parts.
   * 
   * In this handler, the symptoms and the health status are saved to the database.
   * 
   * ATTENTION: This Intent is used also after RegisterHealthStatus Intent. What is absent in that case is
   * the context `check-up-ongoing`.
   * 
   * InputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup`
   * 
   * OutputContext(s) : `none` or StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup`, `check-up-ongoing`
   * @function startCheckUpFlowYesYesHealthStatusSymptomsYesHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv, { symptoms, hurt_body_parts, hurt_body_parts_original }) => {
    conv.data.fallbackCount = 0;

    // Delete duplicates
    symptoms = Array.from(new Set(symptoms));
    hurt_body_parts_original = Array.from(new Set(hurt_body_parts_original));

    // This might be undefined if this Intent is matched after RegisterHealthStatus Intent
    const context = conv.contexts.get('check-up-ongoing');
    let previousSymptoms;
    let health_status_original;
    if (context) {
      health_status_original = context.parameters.health_status_original;
      // Get the list of symptoms (merged) in the context.
      previousSymptoms = context.parameters.totSymptoms;
    } else {
      // If this Intent is matched after RegisterHealthStatus Intent, get the list of symptoms from a session variable.
      previousSymptoms = conv.data.symptomsRegistration;
    }
    // Merge symptoms and hurt body parts
    let totSymptoms = previousSymptoms.concat((symptoms.concat(hurt_body_parts_original)));
    if (totSymptoms.length > 0) {
      if (context) {
        // Save the list of symptoms (merged) in the DB ('check-ups' node).
        await fd.saveCheckUp(conv.user.storage.uid, conv.data.currentSimpleDate, health_status_original, totSymptoms);
      } else {
        // If this Intent is matched after RegisterHealthStatus Intent, save the list of symptoms in the DB ('sintomi' node).
        await fd.saveSymptoms(conv.user.storage.uid, conv.data.currentSimpleDate, totSymptoms);
      }
      cleanContexts(conv);
      ask(conv, dialogs.check_up_step3);
    } else {
      conv.contexts.set('StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup', 4);
      // Reprompt until the user says a symptom or just 'no'/'non ho altri sintomi'.
      if (conv.data.reprompt > 2) {
        conv.close(dialogs.reprompt_symptoms_yes);
      } else {
        ask(conv, dialogs.reprompt_symptoms_yes);
        conv.data.reprompt++;
      }
    }
  });

app.intent('StartCheckUpFlow - yes - yes - healthStatus - symptoms - no',
  /**
   * StartCheckUpFlow - yes - yes - healthStatus - symptoms - no Intent is matched when the user doesn't have
   * other symptoms except from the ones in the `check-up-ongoing` context.
   * 
   * In this handler, the symptoms and the health status (optional) are saved to the database.
   * 
   * ATTENTION: This Intent is used also after RegisterHealthStatus Intent. What is absent in that case is
   * the context `check-up-ongoing`.
   * 
   * InputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup`
   * 
   * OutputContext(s) : `none`
   * @function startCheckUpFlowYesYesHealthStatusSymptomsNoHandler
   * @param {DialogflowConversation} conv conversation object.
   */
  async (conv) => {
    conv.data.fallbackCount = 0;
    // This might be undefined if this Intent is matched after RegisterHealthStatus Intent
    const context = conv.contexts.get('check-up-ongoing');
    if (context) {
      let health_status_original = context.parameters.health_status_original;
      // Get the list of symptoms (merged) in the context.
      let totSymptoms = context.parameters.totSymptoms;
      await fd.saveCheckUp(conv.user.storage.uid, conv.data.currentSimpleDate, health_status_original, totSymptoms);
    } else {
      // If this Intent is matched after RegisterHealthStatus Intent, get the list of symptoms from a session variable.
      await fd.saveSymptoms(conv.user.storage.uid, conv.data.currentSimpleDate, conv.data.symptomsRegistration);
    }
    cleanContexts(conv);
    ask(conv, dialogs.check_up_step3);
  });

app.intent('CheckUpStop',
  /**
   * CheckUpStop is matched only during a check-up when the user says he/she needs to go or stop the check-up. 
   * 
   * InputContext(s) : `check-up-ongoing`
   * 
   * OutputContext(s) : `none`/END CONVERSATION
   * @function checkUpStopHandler
   * @param {DialogflowConversation} conv conversation object.  
   */
  conv => {
    cleanContexts(conv);
    conv.close(format(dialogs.check_up_stop, dialogs.getRandomDialog(dialogs.confirmation), dialogs.getRandomDialog(dialogs.bye)));
  });

//********************************* FALLBACK SECTION **********************************

app.intent('Default Welcome Intent - fallback',
  /**
   * InputContext(s) : `DefaultWelcomeIntent-followup`
   * 
   * OutputContext(s) : `DefaultWelcomeIntent-followup`
   * @function defaultWelcomeIntentFallbackHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    if (conv.data.fallbackCount > 2) {
      conv.close(dialogs.fallback_yes_no_signin[conv.data.fallbackCount]);
    } else {
      ask(conv, dialogs.fallback_yes_no_signin[conv.data.fallbackCount]);
      conv.data.fallbackCount++;
    }
  });


app.intent(['StartCheckUpFlow - fallback', 'StartCheckUpFlow - yes - fallback'],
  /**
   * InputContext(s) : `check-up-prompted`  //// `StartCheckUpFlow-yes-followup`
   * 
   * OutputContext(s) : `check-up-prompted` //// `StartCheckUpFlow-yes-followup`
   * @function startCheckUpFlowFallbackHandler
   * @param {DialogflowConversation} conv conversation object.  
   */
  conv => {
    if (conv.data.fallbackCount > 2) {
      conv.close(dialogs.fallback_yes_no_check_up[conv.data.fallbackCount]);
    } else {
      ask(conv, dialogs.fallback_yes_no_check_up[conv.data.fallbackCount]);
      conv.data.fallbackCount++;
    }
  });

app.intent('StartCheckUpFlow - yes - yes - fallback',
  /**
   * InputContext(s) : `StartCheckUpFlow-yes-yes-followup`
   * 
   * OutputContext(s) : `StartCheckUpFlow-yes-yes-followup`
   * @function startCheckUpFlowYesYesFallbackHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    if (conv.data.fallbackCount > 2) {
      conv.close(dialogs.fallback_health_state_check_up[conv.data.fallbackCount]);
    } else {
      ask(conv, dialogs.fallback_health_state_check_up[conv.data.fallbackCount]);
      conv.data.fallbackCount++;
    }
  });

app.intent('StartCheckUpFlow - yes - yes - healthStatus - fallback',
  /**
   * InputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-followup`
   * 
   * OutputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-followup`
   * @function startCheckUpFlowYesYesHealthStatusFallbackHandler
   * @param {DialogflowConversation} conv conversation object.  
   */
  conv => {
    if (conv.data.fallbackCount > 2) {
      conv.close(dialogs.fallback_symptoms_check_up[conv.data.fallbackCount]);
    } else {
      ask(conv, dialogs.fallback_symptoms_check_up[conv.data.fallbackCount]);
      conv.data.fallbackCount++;
    }
  });

app.intent('StartCheckUpFlow - yes - yes - healthStatus - symptoms - fallback',
  /**
   * InputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup`
   * 
   * OutputContext(s) : `StartCheckUpFlow-yes-yes-healthStatus-symptoms-followup`
   * @function startCheckUpFlowYesYesHealthStatusSymptomsFallbackHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    if (conv.data.fallbackCount > 2) {
      conv.close(dialogs.fallback_yes_no_symptoms[conv.data.fallbackCount]);
    } else {
      ask(conv, dialogs.fallback_yes_no_symptoms[conv.data.fallbackCount]);
      conv.data.fallbackCount++;
    }
  });

app.intent('ListenToMessages - fallback',
  /**
   * InputContext(s) : `unread-messages-prompted`
   * 
   * OutputContext(s) : `unread-messages-prompted`
   * @function listenToMessagesFallbackHandler
   * @param {DialogflowConversation} conv conversation object.  
   */
  conv => {
    if (conv.data.fallbackCount > 2) {
      conv.close(dialogs.fallback_listen_to_messages[conv.data.fallbackCount]);
    } else {
      ask(conv, dialogs.fallback_listen_to_messages[conv.data.fallbackCount]);
      conv.data.fallbackCount++;
    }
  });

//*********************************** HELP SECTION ************************************

app.intent(['StartCheckUpFlow - help', 'StartCheckUpFlow - yes - help'], conv => {
  conv.data.fallbackCount = 0;
  ask(conv, dialogs.help_check_up);
});

//************************************* BYE BYE ***************************************

app.intent('StopConversation',
  /**
   * The StopConversation Intent is matched whenever the user wishes to end the conversation.
   * It can be matched despite the active contexts.
   * 
   * InputContext(s) : `none`
   * 
   * OutputContext(s) : `none`/END CONVERSATION
   * @function stopConversationHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    conv.close(format(dialogs.getRandomDialog(dialogs.come_back_soon), dialogs.getRandomDialog(dialogs.bye)));
  });

//************************************ UTILITIES **************************************

/**
 * Update the therapies in the storage.
 * 
 * @param {String} which type of update (`ALL` -> download therapies from Firestore and update storage, `TODAY` -> update today's therapies based on the therapies in the storage).
 * @param {DialogflowConversation} conv conversation object.
 * @param {String} [userId] UID that identifies the user in Firestore.
 * @param {String} currentDate simple date with the following format `DD/MM/YYYY`.
 * @param {String} currentWeekDay name of the current week day (e.g. `lunedì`).
 * @param {Array<Object>} [therapies] array with the therapies in the node `conv.user.storage.therapies`.
 */
async function updateTherapies(which = 'ALL', conv, userId, currentDate, currentWeekDay, therapies) {
  switch (which) {
    case 'ALL': {
      // Download new therapies and update 'todayTherapies' and 'therapies' nodes
      let therapiesObjects = await fd.getTherapiesFromUser(userId);
      conv.user.storage.therapies = therapiesObjects;
      let todayTherapiesObjects = await ut.getTodayTherapies(therapiesObjects, currentDate, currentWeekDay);
      conv.user.storage.todayTherapies = todayTherapiesObjects;

      // Add timestamp to reduce the number of operations per day (to check if a day has passed)
      conv.user.storage.todayTherapiesTimestap = ud.getFormattedDate(currentDate, ud.justDate);

      // If there are medications already registered under the currentDate in the database,
      // they will be ignored and set to assunto : true in `todayTherapies`.
      var promisesMedications = [];
      for (const t of todayTherapiesObjects) {
        const medications = fd.getMedicationsBasedOnTherapyIdAndDate(userId, t.id, ud.getFormattedDate(currentDate, ud.justDate));
        promisesMedications.push(medications);
      }
      if (promisesMedications.length > 0) {
        let resolvedPromises = await Promise.all(promisesMedications);
        console.log(promisesMedications);

        // Array with all the medications in the DB under currentDay
        let medicationsFromDB = await [].concat.apply([], resolvedPromises);
        console.log(medicationsFromDB);
        conv.user.storage.todayTherapies = await ut.updateTakenMedicationsInTodayTherapiesFromDB(todayTherapiesObjects, medicationsFromDB);
      }
      break;
    }
    case 'TODAY': {
      // Update today's therapies based on the `therapies` node
      let todayTherapiesObjects = await ut.getTodayTherapies(therapies, currentDate, currentWeekDay);
      // Add timestamp to reduce the number of operations per day (to check if a day has passed)
      conv.user.storage.todayTherapiesTimestap = ud.getFormattedDate(currentDate, ud.justDate);
      conv.user.storage.todayTherapies = todayTherapiesObjects;
      break;
    }
    default:
      break;
  }
}

/**
 * This function returns a random string that prompts the user to do a check-up.
 * It also sets the context `check-up-prompted` with a lifespan of 4, and a storage variable
 * `conv.user.storage.lastAskedCheckUp` to the currentDate (so that the next check-up could
 * be prompted after a week).
 * 
 * @param {DialogflowConversation} conv conversation object.
 * @returns a String with a check-up prompt.
 */
function promptWithCheckUp(conv) {
  conv.contexts.set('check-up-prompted', 4);
  // Save the date of the prompt, so that we can reprompt it in a week time (for now)
  conv.user.storage.lastAskedCheckUp = ud.getCurrentDateTime();
  return dialogs.getRandomDialog(dialogs.check_up_prompt);
}

/**
 * This function returns an appropriately formatted string with the medications' remainders.
 * It checks the late medications and puts them in the medications prompt, after setting the context
 * to `taken-meds-prompted`.
 * @param {DialogflowConversation} conv conversation object.
 * @returns a Promise with a resolved String of medications' remainders or `undefined` if there are no remainders.
 */
function promptWithRemainders(conv) {
  return new Promise(resolve => {
    let todaysNotTakenMeds = ut.getTodaysNotTakenMeds(conv.user.storage.todayTherapies, conv.data.currentSimpleDate);
    let medRemindersFromTodayNotTakenMeds = ut.getMedRemindersFromTodayNotTakenMeds(todaysNotTakenMeds, conv.data.currentDate);
    let arrStringMeds = dialogs.getStringFormattedWithMedications(medRemindersFromTodayNotTakenMeds);
    if (arrStringMeds.length > 0) {
      conv.contexts.set('taken-meds-prompted', 4, { medicationsIDs: medRemindersFromTodayNotTakenMeds });
      //resolve(ask(conv, format(dialogs.default_welcome_med_reminder, dialogs.getStringFromArray(arrStringMeds))));
      resolve(dialogs.getStringFromArray(arrStringMeds));
    } else {
      resolve(undefined);
      //resolve(ask(conv, format(dialogs.getRandomDialog(dialogs.greetings), conv.user.storage.firstName)));
    }
  });
}

/**
 * This function constructs the dialog that Miranda will say to the user, to ask him/her to confirm
 * the medications he/she declared to have taken.
 * @param {DialogflowConversation} conv conversation object.
 * @param {Array<Object>} arrConfirmMedications array with the confirmed medications' objects.
 * @param {Array<Object>} medicationsIDs array with all the medications prompted because of lateness.
 * @returns a Promise with the resolved dialog, which is said immediately.
 */
function promptWithMedicationsConfirmation(conv, arrConfirmMedications, medicationsIDs) {
  return new Promise(resolve => {
    // This will contain the dialog that Miranda will say to the user/patient
    let arrStringMeds = [];
    for (const m of arrConfirmMedications) {
      // We shall build the strings for each medication, based on the time of administration (oraAssunzione)
      if (ud.isAfterHour(null, m.ora, m.oraAssunzione) && Math.abs(ud.getDiffBetweenHours(m.ora, m.oraAssunzione) > 1)) {
        // Too early
        arrStringMeds.push(dialogs.getStringFormattedWithMedications([m], 'EARLY'));
      } else if (ud.isBeforeHour(m.ora, m.oraAssunzione) && Math.abs(ud.getDiffBetweenHours(m.ora, m.oraAssunzione) > 1)) {
        // Supposed lateness
        conv.contexts.set('awaiting-confirmation', 4, { confirmedMedications: arrConfirmMedications, totalMedicationsIDs: medicationsIDs, atLeastOneLate: true });
        arrStringMeds.push(dialogs.getStringFormattedWithMedications([m], 'LATE'));
      } else {
        arrStringMeds.push(dialogs.getStringFormattedWithMedications([m]));
      }
    }
    resolve(ask(conv, format(dialogs.taken_list_therapies_confirmation, dialogs.getStringFromArray(arrStringMeds))));
  });
}

/**
 * This function constructs the dialog that Miranda will say to the user, to remind him/her of the
 * medications to be taken today. Of course, there might not be remaining medications.
 * @param {DialogflowConversation} conv conversation object.
 * @returns a Promise with the resolved dialog, which is said immediately.
 */
function promptWithTodaysTherapies(conv) {
  return new Promise(resolve => {
    let todaysNotTakenMeds = ut.getTodaysNotTakenMeds(conv.user.storage.todayTherapies, conv.data.currentSimpleDate);
    if (todaysNotTakenMeds.length > 0) {
      let arrStringMeds = dialogs.getStringFormattedWithMedications(todaysNotTakenMeds);
      resolve(ask(conv, format(
        dialogs.get_therapies_todays_therapies,
        todaysNotTakenMeds.length,
        dialogs.getPluralFromWordAndQuantity('assunzione', todaysNotTakenMeds.length),
        dialogs.getStringFromArray(arrStringMeds)
      )
      ));
    } else {
      resolve(ask(conv, format(dialogs.get_therapies_no_therapies_left, dialogs.getRandomDialog(dialogs.confirmation))));
    }
  });
}

/**
 * This function prompts the user with the number of unread messages and asks the user whether he/she wants
 * to read them all or just the last one.
 * @param {DialogflowConversation} conv conversation object.
 * @param {Array<Object>} unreadMessages array with the unread messages.
 * @returns the dialog that Miranda says.
 */
function promptWithUnreadMessages(conv, unreadMessages) {
  conv.data.fallbackCount = 0;
  conv.contexts.set('unread-messages-prompted', 4);
  return ask(conv, format(dialogs.messages_prompt, unreadMessages.length));
}

/**
 * This function repeats to the user the medication that he/she took.
 * @param {DialogflowConversation} conv conversation object.
 * @param {Object} medFound an object that contains the registered medication.
 * @returns the dialog that Miranda says.
 */
async function promptWithSinglePlannedMedicationConfirmation(conv, medFound) {
  console.log("medfound in promptplanned " + JSON.stringify(medFound));
  let differenceBetweenHours = await ud.getDiffBetweenHours(medFound.ora, medFound.oraAssunzione);
  if (differenceBetweenHours > 1) {
    // Too early
    conv.user.storage.todayTherapies = ut.takeMedicationsById(conv.user.storage.todayTherapies, [medFound]);
    await fd.saveMedicationsTaken(conv.user.storage.uid, [medFound]);
    return ask(conv, format(dialogs.register_single_medication_finished_early, dialogs.getStringFormattedWithMedications([medFound], 'EARLY')));
  } else if (differenceBetweenHours < -1) {
    // Supposed lateness
    conv.user.storage.todayTherapies = ut.takeMedicationsById(conv.user.storage.todayTherapies, [medFound]);
    await fd.saveMedicationsTaken(conv.user.storage.uid, [medFound]);
    return ask(conv, format(dialogs.register_single_medication_finished_late, dialogs.getStringFormattedWithMedications([medFound], 'LATE')));
  } else {
    conv.user.storage.todayTherapies = ut.takeMedicationsById(conv.user.storage.todayTherapies, [medFound]);
    await fd.saveMedicationsTaken(conv.user.storage.uid, [medFound]);
    return ask(conv, format(dialogs.register_single_medication_finished, dialogs.getRandomDialog(dialogs.confirmation), dialogs.getStringFormattedWithMedications([medFound])));
  }
}

/**
 * This function repeats to the user the UNPLANNED medication that he/she took.
 * @param {DialogflowConversation} conv conversation object.
 * @param {String} medication medication's name.
 * @param {String} time the time at which the medication was taken.
 * @param {Number} number the number of medication's units taken.
 * @param {String} medication_type the medication's type (e.g. 'compressa').
 * @returns the dialog that Miranda says.
 */
async function promptWithSingleUnplannedMedicationConfirmation(conv, medication, time, number, medication_type) {
  let medicationObj = ut.buildUnplannedMedication(conv.data.currentSimpleDate, medication, time, number, medication_type);
  await fd.saveUnplannedMedication(conv.user.storage.uid, medicationObj);
  return ask(conv, format(dialogs.register_single_medication_not_planned, medication, dialogs.getStringFormattedWithMedications([medicationObj])));
}

/**
 * This function deletes all the active contexts (except 'authenticated').
 * @param {DialogflowConversation} conv conversation object.
 */
function cleanContexts(conv) {
  // Clean all possible incoming contexts
  for (const c of CONTEXTS) {
    conv.contexts.delete(c);
  }
}

//******************************** REPEAT AND NO INPUT **********************************

app.intent('RepeatIntent',
  /**
   * The RepeatIntent is matched whenever the user wants to hear again the previous prompt.
   * @function repeatIntentHandler
   * @param {DialogflowConversation} conv conversation object.  
   */
  conv => {
    conv.ask(conv.data.lastPrompt);
  });

app.intent('actions_intent_NO_INPUT',
  /**
   * Handles the Dialogflow NO_INPUT intent.
   * Triggered when the user doesn't provide vocal input to the Action. Only works on a smart speaker.
   * @function noInputHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    // This array will hold all the active contexts' names
    var contextsActive = [];
    for (const context of conv.contexts) {
      // The name format is projects/[PROJECT-NAME]/agent/sessions/[SESSION-ID]/contexts/[CONTEXT-NAME-LOWERCASE]'
      var name = context.name;
      // Gets the context's name (in lowercase)
      var trueName = name.match(/([A-Za-z-]*\w$)/);
      if (trueName) {
        contextsActive.push(trueName[1]);
      }
    }
    handleNoInputBasedOnContext(conv, contextsActive);
  });

app.intent('Default Fallback Intent',
  /**
   * Handles the Default Fallback Intent, which is matched when the user says something
   * that cannot be associated with any of the Intents and cannot be catched by any of
   * the fallback Intents provided.
   * @function noInputHandler
   * @param {DialogflowConversation} conv conversation object. 
   */
  conv => {
    if (conv.data.fallbackCount > 2) {
      conv.close(dialogs.fallback_plus_replay[conv.data.fallbackCount]);
    } else {
      conv.ask(format(dialogs.fallback_plus_replay[conv.data.fallbackCount], conv.data.lastPrompt));
      conv.data.fallbackCount++;
    }
  });

/**
 * This function is called whenever the `actions_intent_NO_INPUT` Intent is matched.
 * After checking which contexts are active, the function gets the `REPROMPT_COUNT`,
 * which is a counter for unspoken inputs and makes a call to `repromptNoInputBasedOnCount`.
 * 
 * @param {DialogflowConversation} conv conversation object.
 * @param {Array<String>} contexts array with the active contexts' names (in lower case).
 */
async function handleNoInputBasedOnContext(conv, contexts) {
  let repromptCount = parseInt(conv.arguments.get('REPROMPT_COUNT'));
  if (contexts.includes('check-up-prompted') || contexts.includes('startcheckupflow-yes-followup')) {
    repromptNoInputBasedOnCount(conv, repromptCount, dialogs.fallback_yes_no_check_up);
  } else if (contexts.includes('startcheckupflow-yes-yes-followup')) {
    // Waiting for health status
    repromptNoInputBasedOnCount(conv, repromptCount, dialogs.fallback_health_state_check_up);
  } else if (contexts.includes('startcheckupflow-yes-yes-healthstatus-followup')) {
    // Waiting for symptoms
    repromptNoInputBasedOnCount(conv, repromptCount, dialogs.fallback_symptoms_check_up);
  } else if (contexts.includes('startcheckupflow-yes-yes-healthstatus-symptoms-followup')) {
    // Waiting for symptoms or negation/confirmation
    repromptNoInputBasedOnCount(conv, repromptCount, dialogs.fallback_yes_no_symptoms);
  } else if (contexts.includes("taken-meds-prompted")) {
    let repromptCount = parseInt(conv.arguments.get('REPROMPT_COUNT'));
    let listOfRemainders = await promptWithRemainders(conv);
    repromptNoInputBasedOnCount(conv, repromptCount, dialogs.fallback_not_taken_medications, true, listOfRemainders);
  } else if (contexts.includes('takenmedications-whichone-followup') && contexts.includes('awaiting-confirmation') && contexts.includes('awaiting-time')) {
    repromptNoInputBasedOnCount(conv, repromptCount, dialogs.fallback_plus_replay, true, conv.data.lastPrompt);
  } else {
    // In every other case
    repromptNoInputBasedOnCount(conv, repromptCount, dialogs.fallback_plus_replay, true, conv.data.lastPrompt);
  }
}

/**
 * This function selects the appropriate dialog based on the number of 'silences' from the user and
 * the array of strings passed as a parameter in `handleNoInputBasedOnContext`.
 *
 * @param {DialogflowConversation} conv conversation object.
 * @param {Number} repromptCount counter for unspoken inputs.
 * @param {Array<String>} dialogs array of strings, from which the no-input prompt will be extracted.
 * @param {Boolean} [formatted] `true` - if `dialogs` contains placeholders like `{0}`; `false` - if `dialogs` doesn't contain placeholders.
 * @param {String} formattedParam a string to be substituted to the placeholder (if present) in `dialogs`.
 */
function repromptNoInputBasedOnCount(conv, repromptCount, dialogs, formatted = false, formattedParam) {
  if (formatted) {
    if (repromptCount === 0) {
      conv.ask(format(dialogs[repromptCount + 1], formattedParam));
    } else if (repromptCount === 1) {
      conv.ask(format(dialogs[repromptCount + 1], formattedParam));
    } else if (conv.arguments.get('IS_FINAL_REPROMPT')) {
      conv.close(dialogs[repromptCount + 1]);
    }
  } else {
    if (repromptCount === 0) {
      conv.ask(dialogs[repromptCount + 1]);
    } else if (repromptCount === 1) {
      conv.ask(dialogs[repromptCount + 1]);
    } else if (conv.arguments.get('IS_FINAL_REPROMPT')) {
      conv.close(dialogs[repromptCount + 1]);
    }
  }
}

/** Set the DialogflowApp object to handle the HTTPS POST request */
exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);