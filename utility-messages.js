/**
 * A module that deals with messages.
 * @module utility-messages
 * @author Alessandra Semeraro
 */

const fd = require('./firebase-db');

module.exports = {
    /**
     * Checks whether there are unread messages in the database.
     * 
     * @param {String} userId user's UID.
     * @returns an Array with the messages or `undefined` if no messages were found.
     */
    checkUnreadMessages : async function(userId) {
        const messages = await fd.getDoctorMessages(userId);
        if(messages.length > 0) {
            // There's at least one unread message
            return messages;
        } else {
            return undefined;
        }
    }
}