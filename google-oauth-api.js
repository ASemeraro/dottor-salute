/**
 * Deals with the user's Google Profile informations.
 * @module google-oauth-api
 * @author Alessandra Semeraro
 */

const rp = require('request-promise');

/**
 * Gets the user's Google Profile informations.
 * @param {String} accessToken user's access token resulting from a successfull sign-in.
 * @returns a Promise with the resolved JSON body.
 */
function getUserProfile(accessToken) {
    // Set the options for the API call
    var options = {
        uri: 'https://www.googleapis.com/oauth2/v1/userinfo',
        qs: {
            access_token: accessToken // -> uri + '?access_token=xxxxx%20xxxxx'
        },
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };
    
    return rp(options);
}

module.exports.getUserProfile = getUserProfile;
