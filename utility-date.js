/**
 * A module that wraps up all the methods related to the manipulation of a date string.
 * @module utility-date
 * @author Alessandra Semeraro
 */

const moment = require('moment-timezone');

module.exports = {

	/** Full date (e.g. sabato 13 ottobre 2018 12:31) */
	fullDateTime: 'LLLL',
	/** Standard date DD/MM/YYYY (e.g. 13/10/2018) */
	justDate: 'L',
	/** Standard hour with 24h format HH/mm (e.g. 12:31) */
	justTime: 'LT',
	/** Standard day (e.g. sabato) */
	justDay: 'dddd',
	/** Offset of a day */
	offsetTypeDay: 'd',
	/** Offset of a week */
	offsetTypeWeek: 'w',

	/**
	 * For now it works only in Italy.
	 * To make it work globally we need the user's coordinates, to calculate the timezone.
	 */
	timeZone: 'Europe/Rome',
	/** For now it works only in Italy. */
	locale: 'it',

	/**
	 * Gets the current time/date based on the locale, the timezone and the format provided.
	 * 
	 * @param {string} [dateFormat] the string's format that `moment().format()` returns.
	 */
	getCurrentDateTime: function (dateFormat) {
		moment.locale(this.locale);
		if (!dateFormat) {
			return moment().tz(this.timeZone).format();
		} else {
			return moment().tz(this.timeZone).format(dateFormat);
		}
	},

	/**
	 * Calculates the difference between two given hours.
	 * 
	 * @param {String} a the first hour.
	 * @param {String} b the second hour.
	 * @returns a Number with the calculated difference.
	 */
	getDiffBetweenHours: function (a, b) {
		var d1 = moment(a, "HH:mm");
		var d2 = moment(b, "HH:mm");
		return d1.diff(d2, 'hours');
	},

	/**
	 * Compares two dates to know if they are the same.
	 * 
	 * @param {string} date the first date.
	 * @param {string} date2 the second date.
	 * @returns a Boolean - `true` if the hour is the same, `false` otherwise.
	 */
	isSameDate: function (date, date2) {
		return moment(date).isSame(date2);
	},

	/**
	 * Checks whether a certain amount of time has passed from the given date.
	 * 
	 * @param {string} date the first date.
	 * @param {string} date2 the date from which we need to count whether a certain amount of time has passed.
	 * @param {number} [offsetValue] the amount of time we need to add to 'date2'.
	 * @param {string} [offsetType] the type of units we need to add to 'date2' (e.g. 'd' -> day).
	 * @returns a Boolean - `true` if the first date is the same or comes after the second one, `false` otherwise.
	 */
	isSameOrAfterDate: function (date, date2, offsetValue, offsetType) {
		if (!offsetValue && !offsetType) {
			var d = moment(date).tz(this.timeZone).format(this.justDate);
			var d1 = moment(d, "DD/MM/YYYY").tz(this.timeZone);
			var d2 = moment(date2, "DD/MM/YYYY").tz(this.timeZone);
			return d1.isSameOrAfter(d2);
		} else {
			var newDate = moment(date2).add(offsetValue, offsetType);
			return moment(date).isSameOrAfter(newDate);
		}
	},

	/**
	 * Checks whether the first date has surpassed the given second date.
	 * 
	 * @param {String} date the first date.
	 * @param {String} date2 the date to compare the first one to.
	 * @returns a Boolean - `true` if the first date comes after the second one, `false` otherwise.
	 */
	isAfterDate: function (date, date2) {
		var d = moment(date).tz(this.timeZone).format(this.justDate);
		var d1 = moment(d, "DD/MM/YYYY").tz(this.timeZone);
		var d2 = moment(date2, "DD/MM/YYYY").tz(this.timeZone);
		return d1.isAfter(d2);
	},

	/**
	 * Checks whether the first date precedes (or is the same as) the given second date.
	 * 
	 * @param {String} date the current date.
	 * @param {String} date2 the date to compare the first one to.
	 * @returns a Boolean - `true` if the first date is the same or comes before the second one, `false` otherwise.
	 */
	isSameOrBeforeDate: function (date, date2) {
		var d = moment(date).tz(this.timeZone).format(this.justDate);
		var d1 = moment(d, "DD/MM/YYYY").tz(this.timeZone);
		var d2 = moment(date2, "DD/MM/YYYY").tz(this.timeZone);
		return d1.isSameOrBefore(d2);
	},

	/**
	 * Checks whether the currentDate's hour has surpassed the given hour. Or it could check whether
	 * the first hour comes after the second.
	 * 
	 * @param {String} currentDate current full date.
	 * @param {String} hour the hour to compare the currentDate's hour to.
	 * @param {String} [hour2] the second hour (if provided) to compare the first one to.
	 * @returns a Boolean - `true` if the first hour comes after the second one, `false` otherwise.
	 */
	isAfterHour: function (currentDate, hour, hour2) {
		if (!hour2) {
			var h = moment(currentDate).tz(this.timeZone).format(this.justTime);
			var h1 = moment(h, "HH:mm").tz(this.timeZone);
			var h2 = moment(hour, "HH:mm").tz(this.timeZone);
			return h1.isAfter(h2);
		} else {
			var ho1 = moment(hour, "HH:mm").tz(this.timeZone);
			var ho2 = moment(hour2, "HH:mm").tz(this.timeZone);
			return ho1.isAfter(ho2);
		}

	},

	/**
	 * Checks whether the first hour comes before the given second hour.
	 * 
	 * @param {String} hour the first hour.
	 * @param {String} hour2 the second hour.
	 * @returns a Boolean - `true` if the first hour comes before the second one, `false` otherwise.
	 */
	isBeforeHour: function (hour, hour2) {
		var h1 = moment(hour, "HH:mm").tz(this.timeZone);
		var h2 = moment(hour2, "HH:mm").tz(this.timeZone);
		return h1.isBefore(h2);
	},

	/**
	 * Checks whether the first hour comes before (or is the same as) the given second hour.
	 * 
	 * @param {String} hour the first hour.
	 * @param {String} hour2 the second hour.
	 * @returns a Boolean - `true` if the first hour is the same or comes before the second one, `false` otherwise.
	 */
	isSameOrBeforeHour: function (hour, hour2) {
		var h1 = moment(hour, "HH:mm").tz(this.timeZone);
		var h2 = moment(hour2, "HH:mm").tz(this.timeZone);
		return h1.isSameOrBefore(h2);
	},

	/**
	 * Checks whether the first hour is the same as the second one.
	 * 
	 * @param {String} hour the first hour.
	 * @param {String} hour2 the second hour.
	 * @returns a Boolean - `true` if the first hour is the same as the second one, `false` otherwise.
	 */
	isSameHour: function (hour, hour2) {
		var h1 = moment(hour, "HH:mm").tz(this.timeZone);
		var h2 = moment(hour2, "HH:mm").tz(this.timeZone);
		return h1.isSame(h2);
	},

	/**
	 * Gets a string with the appropriately formatted date. The format is basically the granularity,
	 * which determines the date's substring to be returned.
	 *      
	 * @param {String} date ISO date.
	 * @param {String} dateFormat the granularity of the resulting string.
	 * @returns a String with the appropriately formatted date.
	 */
	getFormattedDate: function (date, dateFormat) {
		return moment(date).tz(this.timeZone).format(dateFormat);
	}
}
